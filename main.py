#!/usr/bin/env python

import glob, datetime, time, multiprocessing
from zipfile import ZipFile


def scan_compteur(path, q):
    # count useful data
    nbrDir = nbrTxt = other = 0
    print("Process 1 starts: ")
    time.sleep(1)

    nbrDir = len(glob.glob(path+'**/*/', recursive=True))
    nbrTxt = len(glob.glob(path+'**/*.txt', recursive=True))
    other = len(glob.glob(path+'**/*.*', recursive=True)) - nbrTxt

    q.put("Total Number of sub dir : "+str(nbrDir))
    q.put("Total Number of .txt file : "+str(nbrTxt))
    q.put("Others files : "+str(other))


def scan_archive(path, q):
    # scan dir and create zip file
    now = datetime.datetime.now()
    print("Process 2 starts: ")
    time.sleep(1)

    # convert time into a string
    today = now.strftime("%m_%d_%Y-%H_%M_%S")

    # define the file name
    nom_zipfile = 'backup'+today+'.zip'

    # add the last backup date in the queue
    q.put("Last Backup : "+today)

    # collect of .txt file of dir
    files = glob.glob(path+'**/*.txt', recursive=True)

    # create backup file for each .txt file
    for data in files:
        with ZipFile(nom_zipfile, 'w') as zp:
            zp.write(data)


def print_infos(path, q):
    # to print info of dir
    print("Process 3 starts: ")
    time.sleep(1)
    print("*"*50, "\nInfos on dir: ", path)
    while not q.empty():
        print(q.get())


if __name__ == '__main__':

    # create multiprocessing Queue
    q = multiprocessing.Queue()
    path = "test/"

    # create processes
    p1 = multiprocessing.Process(target=scan_compteur, args=(path, q))
    p2 = multiprocessing.Process(target=scan_archive, args=(path, q))
    p3 = multiprocessing.Process(target=print_infos, args=(path, q))

    # execute process p1, p2, and wait until they are executed
    p1.start()
    p2.start()

    p1.join()
    p2.join()

    # execute process p3
    p3.start()